import ApexCharts from 'apexcharts'

export default (context, inject) => {
  inject('apexchart', ApexCharts)
}
