import * as date from 'date-fns'

export default (context, inject) => {
  inject('date', date)

  const months = [
    'มกราคม',
    'กุมภาพันธ์',
    'มีนาคม',
    'เมษายน',
    'พฤษภาคม',
    'มิถุนายน',
    'กรกฎาคม',
    'สิงหาคม',
    'กันยายน',
    'ตุลาคม',
    'พฤศจิกายน',
    'ธันวาคม',
  ]

  const weekdays = [
    'อาทิตย์',
    'จันทร์',
    'อังคาร',
    'พุธ',
    'พฤหัสบดี',
    'ศุกร์',
    'เสาร์',
  ]

  inject('months', months)

  inject('weekdays', weekdays)

  inject('getYear', (year) => year + 543)

  inject('formatApi', (_date) => {
    return date.format(_date, `yyyy-MM-dd'T'HH:mm:ss.SSSxxx`)
  })

  inject('thFormat', (_date) => {
    return `วัน${weekdays[_date.getDay()]}ที่ ${_date.getDate()} ${
      months[_date.getMonth()]
    } ${_date.getFullYear() + 543}`
  })
}
