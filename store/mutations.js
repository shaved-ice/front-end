export default {
  UPDATE_EVENT_ACTION(state, action) {
    state.dialogType = action.type
    state.eventActionId = action.id
  },

  OPEN_DIALOG(state, type) {
    state.dialogType = type
  },

  CLOSE_DIALOG(state) {
    state.dialogType = null
  },

  SET_CURRENT_PLANNER(state, planner) {
    state.planner = planner
  },
}
