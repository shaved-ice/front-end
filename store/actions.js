export default {
  updateEventAction({ commit }, action) {
    commit('UPDATE_EVENT_ACTION', action)
  },

  openDialog({ commit }, type) {
    commit('OPEN_DIALOG', type)
  },

  closeDialog({ commit }) {
    commit('CLOSE_DIALOG')
  },

  async fetchPlanner({ commit }, plannerId) {
    try {
      const { data } = await this.$axios.post(
        '/v2/planner/getById',
        {
          planner_id: plannerId,
        },
        {
          headers: {
            Authorization: this.$auth.strategy.token.get(),
          },
        }
      )

      const now = this.$date.startOfDay(new Date()).getTime()
      const plannerDate = new Date(data.planner.date).getTime()

      const finished = data.planner.is_finished > 0
      const isPast = now > plannerDate

      data.planner.isPast = isPast

      data.planner.finished = finished || isPast
      data.planner.date = new Date(data.planner.date)

      console.log(data)

      commit('SET_CURRENT_PLANNER', data.planner)
    } catch (e) {
      console.log(e)
    }
  },
}
