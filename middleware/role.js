export default function (context) {
  // const options = context.route.matched[0].components.default.options
  const auth = context.$auth

  if (
    auth &&
    auth.loggedIn &&
    auth.user.role_id === 1 &&
    !context.route.name.startsWith('admin')
  ) {
    context.redirect('/admin')
  }

  if (
    auth &&
    auth.loggedIn &&
    auth.user.role_id !== 1 &&
    context.route.name.startsWith('admin')
  ) {
    context.redirect('/')
  }
}
